package hu.braininghub.bh06.notes2.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Date;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DefaultNoteDAO implements NoteDAO{
	
	
	
	@Resource (lookup = "jdbc/_notes2")
	private DataSource ds;
	
	@Resource
	private UserTransaction ut;
	

	@Override
	public void add(String title, String text, String date, Status status) {
		try (PreparedStatement stmt =ds.getConnection().prepareStatement("INSERT INTO NOTES2 (TITLE, TEXT, DUE_DATE, STATUS) VALUES (?,?,?,?)")){			
			ut.begin();
			stmt.setString(1, title);
			stmt.setString(2, text);
			stmt.setDate(3, java.sql.Date.valueOf(date));
			stmt.setString(4, "UNCHECKED");
			stmt.executeUpdate();
			ut.commit();
		} catch (Exception e) {
			try {
				ut.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			
		}
		
	}

	@Override
	public void delete(String id) {
		//Connection con=null;
		
		try (PreparedStatement pstmt =ds.getConnection().prepareStatement("DELETE FROM NOTES2 WHERE ID = ?")){			//WHERE ID = ?
			ut.begin();
			pstmt.setString(1, id);
			pstmt.executeUpdate();
			ut.commit();
		} catch (Exception e) {
			
			e.printStackTrace();
			
		}
	}

	@Override
	public void update(String id, String text) {
		try (PreparedStatement stmt =ds.getConnection().prepareStatement("UPDATE NOTES2 SET TEXT=? WHERE ID=?")){			
			ut.begin();
			stmt.setString(1, text);
			stmt.setString(2, id);
			stmt.executeUpdate();
			ut.commit();
		} catch (Exception e) {
			try {
				ut.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			
		}
	}

	@Override
	public void read(String id) {
		try (PreparedStatement stmt =ds.getConnection().prepareStatement("SELECT * FROM NOTES2 WHERE ID=?")){			
			ut.begin();
			
			stmt.setString(1, id);
			stmt.executeUpdate();
			ut.commit();
		} catch (Exception e) {
			try {
				ut.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			
		}
	}

	@Override
	public void update(String id, Status status) {
		try (PreparedStatement stmt =ds.getConnection().prepareStatement("UPDATE NOTES2 SET STATUS=? WHERE ID=?")){			
			ut.begin();
			stmt.setString(1, "CHECKED");
			stmt.setString(2, id);
			stmt.executeUpdate();
			ut.commit();
		} catch (Exception e) {
			try {
				ut.rollback();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			
		}
		
	}

}

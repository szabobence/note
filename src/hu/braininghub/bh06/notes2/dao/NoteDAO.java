package hu.braininghub.bh06.notes2.dao;

import java.sql.Date;
import java.sql.SQLException;

public interface NoteDAO {
	
	void add(String title, String text, String date, Status status);
	
	void delete(String id);
	
	void update(String id, String text);
	
	void update(String id, Status status);
	
	void read(String id);
	
}

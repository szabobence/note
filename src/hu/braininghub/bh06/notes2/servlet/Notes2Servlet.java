package hu.braininghub.bh06.notes2.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hu.braininghub.bh06.notes2.dao.NoteDAO;
import hu.braininghub.bh06.notes2.dao.Status;

@WebServlet(urlPatterns = "/Notes2Servlet")
public class Notes2Servlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private NoteDAO note;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
        PrintWriter out = resp.getWriter(); 
		
		Connection con=null;
		
		try (PreparedStatement ps=con.prepareStatement("select * from notes2 where id=?")){
			
			Class.forName("oracle.jdbc.driver.OracleDriver");

            con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","chelseafc52");               

            //PreparedStatement ps=con.prepareStatement("select * from notes2 where id=?");
             ps.setString(1, req.getParameter("id"));
            //out.print("<table width=25% border=1>");

            //out.print("<center><h1>Notes:</h1></center>");

            ResultSet rs=ps.executeQuery();                

            /* Printing column names */

            //ResultSetMetaData rsmd=rs.getMetaData();
            
            //out.print("<tr>");
            //out.print("<td>"+rsmd.getColumnName(2)+"</td>" + "<td>" + rsmd.getColumnName(3) + "</td>"+ "<td>" + rsmd.getColumnName(4) +"</td>"+ "<td>" + rsmd.getColumnName(5) +"</td></tr>");
            out.print("<tr><td>"+rs.getString(2)+"</td>" + "<td>" +rs.getString(3)+ "</td>" + "<td>"+rs.getDate(4) + "</td>"+ "<td>" +rs.getString(5)+"</td></tr>");


            //out.print("</table>");
			
		}
		catch(Exception ex) {
			
			ex.printStackTrace();
		}
		
		
		
		
		
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String title = req.getParameter("title");
		System.out.println("title: " + title);
		String text = req.getParameter("text");
		String date = req.getParameter("date");
		Status status = Status.valueOf(req.getParameter("status"));
		note.add(title, text, date, status);
		//req.getRequestDispatcher("/NotesServletDoGet.jsp").forward(req, resp);
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String id = req.getParameter("id");
		note.delete(id);
		
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String id = req.getParameter("id");
		String text = req.getParameter("text");
		note.update(id, text);
	}
	
	
	

}
